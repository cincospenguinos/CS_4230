# Andre LaFleur's CS 4230 Class Project

## What is it?

To parallelize an information extraction tool that detects opinionated sentences in news articles and presents information about them.

The repository is [here](https://github.com/cincospenguinos/topaz_turtle).

## Why is this helpful?

Tasks in NLP tend to be computationally expensive. Many NLP projects do a variety of things that make it time consuming and costly to
tackle a given task. My project would try to find a way to make this task move a lot faster simply by taking advantage of the multiple
cores a machine has.

## What would I need?

* A machine with multiple cores (like the CADE machines)
* A multithreaded library for Java, like [java.util.concurrent](https://docs.oracle.com/javase/6/docs/api/java/util/concurrent/package-summary.html), or even [Quartz](http://www.quartz-scheduler.org/)
* Evaluation tools for Java (I don't know of any yet)
* A positive, forward-thinking attitude