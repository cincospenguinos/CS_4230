/**
 * Andre LaFleur's Primes v1
 *
 * This program uses OpenMP to find all primes beneath some range. I modified the code provided
 * by Ganesh and used a critical section. I'll include another that doesn't rely on a critical
 * section. That said, I have yet to see std::vector<> have a datarace without the lock.
 */
#include <iostream> 
#include <cmath> 
#include <stdlib.h>
#include <omp.h> 
#include <vector>
#include <algorithm>

int main(int argc, char* argv[]) {
	if (argc < 3) {
		std::cout << "usage: " << std::endl;
		exit(0);
	}

	int thread_count = strtol(argv[1], NULL, 10);
	int primes_range = strtol(argv[2], NULL, 10);

	std::vector<int> primes_parallel = std::vector<int>();
	std::vector<int> primes_series = std::vector<int>();

	#pragma omp parallel num_threads(thread_count)
	{
		int i;
		#pragma omp for schedule(dynamic, 1000000)
		for(i = 3; i <= primes_range; i += 2) { 
			int limit = (int) sqrt((float) i) + 1; // This is the limit that we are searching up towards

			int j = 3;
			bool isPrime = true;

			while (j < limit && isPrime) {
				if (i % j == 0)
					isPrime = false;
				else
					j++;
			}

			if (isPrime)
				primes_parallel.push_back(i);
		}
	}

	// Serial version to check and make sure we're good
	for(int i = 3; i <= primes_range; i += 2) { 
		int limit = (int) sqrt((float) i) + 1; // This is the limit that we are searching up towards

		int j = 3;
		bool isPrime = true;

		while (j < limit && isPrime) {
			if (i % j == 0)
				isPrime = false;
			else
				j++;
		}

		if (isPrime) {
			#pragma omp critical
			primes_parallel.push_back(i);
		}
	}

	int mistakes = 0;
	for(std::vector<int>::iterator it = primes_series.begin(); it != primes_series.end(); ++it) {
		if (std::find(primes_parallel.begin(), primes_parallel.end(), *it) == primes_parallel.end()) {
			mistakes ++;
		}
	}

	for (std::vector<int>::iterator it = primes_parallel.begin(); it != primes_parallel.end(); ++it)
		std::cout << *it << std::endl;

	// std::cout << "There were " << mistakes << " mistakes." << std::endl;
}