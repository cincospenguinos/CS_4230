#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>
#include <cstdio>
#include "timer.h"

// The arrays we will interact with
double *A;
double *B;
double *C;
double *D;

// For timing
double start;
double end;

// Defining aspects of the problem
int probsize;
int threshold;
int threads;

void sanity_check() {
	// Here's the serial version to check our work
	for(int i=0; i < probsize; ++i) {
		D[i] = A[i] * B[i];
	}

	int failures = 0;

	for (int i = 0; i < probsize; i++) {
		if (C[i] != D[i]) {
			printf("Missed %d\n", i);
			failures ++;
		}
	}

	if (failures == 0)
		printf("Everything's good!\n");
	else
		printf("%d total failures\n", failures);
}

int main(int argc, char **argv) {
	if (argc != 4) {
		std::cout << "Usage: openmp_dp <probsize> <threshold> <threads>" << std::endl;
		return 0;
	}

	probsize = std::stoi(argv[1]);
	threshold = std::stoi(argv[2]);
	threads = std::stoi(argv[3]);

	A = (double *) malloc(probsize * sizeof(double *));
	B = (double *) malloc(probsize * sizeof(double *));
	C = (double *) malloc(probsize * sizeof(double *));
	D = (double *) malloc(probsize * sizeof(double *));

	// Fill the arrays
	srand(17); //Seed with 17
	for (int i=0; i < probsize; ++i) {
	    A[i] = rand()%16;
		B[i] = rand()%16;
	}

	for (int i = 0; i < probsize; i++)
		C[i] = 0;

	int abs_low = 0;
	int abs_high = probsize - 1;

	GET_TIME(start);
	#pragma omp parallel num_threads(threads)
	{
		#pragma omp parallel for schedule(dynamic, threshold)
		for (int i = 0; i < probsize; i++)
			C[i] = A[i] * B[i];
	}
	GET_TIME(end);

	// sanity_check();

	printf("%f", end - start);


	free(A);
	free(B);
	free(C);
	free(D);


	return 0;
}
