/**
 * merge_sort.cpp
 *
 * TBB version of merge sort. This is going to be fun.
 *
 */
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <tbb/tbb.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>
#include "timer.h"

#define MERGE_SORT_LIMIT 64 // We may want to change this

typedef int T;

T *source_array;
T *answer;

int prob_size; // The size of our arrays
int threads; // The number of threads we're using

void sanity_check() {
	int mistakes = 0;
	for (int i = 0; i < prob_size; i++) {
		std::cout << source_array[i];

		if (source_array[i] != answer[i]) {
			mistakes++;
			std::cout << "\t*";
		}

		std::cout << std::endl;
	}

	std::cout << mistakes << " mistakes were made." << std::endl;
}

// This is from merge_tbb.h, but I included it here so I only have to include one file
// merge sequences [xs,xe) and [ys,ye) to output [zs,(xe-xs)+(ye-ys)
void parallel_merge( const T* xs, const T* xe,  const T* ys, const T* ye, T* zs ) {
    const size_t MERGE_CUT_OFF = 2000;
    if( xe-xs + ye-ys <= MERGE_CUT_OFF ) {
        std::merge(xs,xe,ys,ye,zs);
    } else {
        const T *xm, *ym;
        if( xe-xs < ye-ys  ) {
            ym = ys+(ye-ys)/2;
            xm = std::upper_bound(xs,xe,*ym);
        } else {
            xm = xs+(xe-xs)/2;
            ym = std::lower_bound(ys,ye,*xm);
        }
        T* zm = zs + (xm-xs) + (ym-ys);
        tbb::parallel_invoke( [=]{parallel_merge( xs, xm, ys, ym, zs );},
                              [=]{parallel_merge( xm, xe, ym, ye, zm );} );
    }
}

// sorts [xs,xe).  zs[0:xe-xs) is temporary buffer supplied by caller.
// result is in [xs,xe) if inplace==true, otherwise in zs[0:xe-xs)
void parallel_merge_sort( T* xs, T* xe, T* zs, bool inplace ) {
    const size_t SORT_CUT_OFF = 500;
    if(xe - xs <= SORT_CUT_OFF) {
        std::stable_sort( xs, xe );
        if( !inplace ) 
            std::move( xs, xe, zs );
    } else {
       T* xm = xs + (xe-xs)/2;
       T* zm = zs + (xm-xs);
       T* ze = zs + (xe-xs);
       tbb::parallel_invoke( [=]{parallel_merge_sort( xs, xm, zs, !inplace );},
                             [=]{parallel_merge_sort( xm, xe, zm, !inplace );} );
       if( inplace )
           parallel_merge( zs, zm, zm, ze, xs );
       else
           parallel_merge( xs, xm, xm, xe, zs );
   }
}

void call_parallel_merge_sort( T* xs, T* xe ) {
    T* zs = new T[xe-xs];
    parallel_merge_sort( xs, xe, zs, true );
    delete[] zs;
}


int main(int argc, char **argv) {
	if (argc != 3) {
		std::cout << "Usage: ./merge_sort <problem size> <threads>" << std::endl;
		return 0;
	}

	prob_size = std::stoi(argv[1]);
	threads = std::stoi(argv[2]);

	source_array = new T[prob_size];
	answer = new T[prob_size];

	// Create a random array
	srand(17);
	for (int i = 0; i < prob_size; i++) {
		int number = rand()%32768;
		source_array[i] = number;
		answer[i] = number;
	}

	// Sort the answer array so we will know if mergesort works or not
	std::sort(answer, answer + sizeof answer / sizeof answer[0]);

	// Now let's merge sort it up!
	tbb::task_scheduler_init init(threads); // Get the threads we need
	double start_time, end_time;

	// std::cout << "Starting merge sort...";
	GET_TIME(start_time);
	call_parallel_merge_sort(source_array, source_array + sizeof source_array / sizeof source_array[0]);
	GET_TIME(end_time);
	// std::cout << "done." << std::endl;

	// sanity_check();

	// std::cout << "It took " << (end_time - start_time) << " seconds to sort." << std::endl;
	std::cout << (end_time - start_time);

	// Make sure we free those resources before calling it quits!
	delete [] source_array;
	delete [] answer;

	return 0;
}