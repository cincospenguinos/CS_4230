/**
 * tbb_dp.cpp
 */
#include <iostream>
#include <cstdio>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <tbb/tbb.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>
#include "timer.h"

// The arrays we will interact with
double *A;
double *B;
double *C;
double *D;

// For timing
double start;
double end;

// Defining aspects of the problem
int probsize;
int threshold;
int threads;

int sanity_check() {
	// Here's the serial version to check our work
	for(int i=0; i < probsize; ++i) {
		D[i] = A[i] * B[i];
	}

	int failures = 0;

	for (int i = 0; i < probsize; i++) {
		if (C[i] != D[i]) {
			// printf("Missed %d\n", i);
			failures ++;
		}
	}

	return failures;
}

int main(int argc, char **argv) {
	if (argc != 4) {
		std::cout << "Usage: openmp_dp <probsize> <threshold> <threads>" << std::endl;
		return 0;
	}

	probsize = std::stoi(argv[1]);
	threshold = std::stoi(argv[2]);
	threads = std::stoi(argv[3]);

	A = new double[probsize];
	B = new double[probsize];
	C = new double[probsize];
	D = new double[probsize];

	srand(17); //Seed with 17
	for (int i=0; i < probsize; ++i) {
	    A[i] = rand()%16;
		B[i] = rand()%16;
	}

	for (int i = 0; i < probsize; i++)
		C[i] = 0;

	int abs_low = 0;
	int abs_high = probsize - 1;

	// Now the dot product
	// int num_threads = tbb::task_scheduler_init::default_num_threads();
	// for (int p = 1; p <= num_threads; p++) {
		tbb::task_scheduler_init init(threads);

		GET_TIME(start);
		tbb::parallel_for(tbb::blocked_range<size_t>(0, probsize),
			[=](const tbb::blocked_range<size_t>& r) {
				for (size_t i = r.begin(); i != r.end(); i++)
					C[i] = A[i] * B[i];
			}
			);
		GET_TIME(end);

		int errors = sanity_check();
		if (errors) {
			std::cout << "It didn't work!" << std::endl;
			return 1;
		}

		std::cout << (end - start);
	// }

	delete [] A;
	delete [] B;
	delete [] C;
	delete [] D;


	return 0;
}